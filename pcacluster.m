function [ldPC,ldScores,grpIdG,f] = pcacluster(datatable,grpId)
%pcacluster (part of PCATools)
% By Kevin Mader
% Updated 08-15-07
%This function is made to cluster the data using PCA-LDA and using Tyler's
%Method for determining the number of PCs to use
%
%Input Variables
% datatable [files x numberOfWavelengs] this is the spectral data
% grpId [files x 1] this is the classification assigned in the program
% (groundTruth)
%
%Output Variables
% ldComp [numberOfWavelengs x numOfPC] the components used to plot the data
% (eigenvectors)
% ldScores [numOfPC x files] the score table from the analysis
% (eigenvalues)
% grpIdG [files x 1] the classification determined by this algorithm
% f is the classifier object, i just export it so the graph can show the
% decision regions
numObs=size(datatable,1);
longTylerRun=0; % don't run the full blown tyler code just use the first 15 components
Folds=[10]; % 10-Fold Analysis rather than every point (to run every point leave folds =[];

grpCnt=[];
for i=1:max(grpId)
    grpCnt(i)=length(find(grpId==i)); % makes a list of the number of elements in each group
end
maxComp=min([30 grpCnt/2]); % use up to 30 or the smallest number of elements in a group/2 - PCs at the same time

scoreSave=zeros(size(datatable,2),1);
j=1;
[coeff score]=princomp(datatable);
if longTylerRun==1
    h=figure;
    while length(find(scoreSave))<maxComp
        scoreSave(j)=1; % save the current score

        coeffs=coeff(:,logical(scoreSave)); % delete the extra components
        scores=score(:,logical(scoreSave)); % delete the extra scores

        f=lda(scores,grpId);
        [ldPC,b]=cvar(f);
        ldScores=scores*ldPC;
        [grpIdG,pScore]=tycrossval('lda',datatable,grpId,Folds,scoreSave);
        if size(ldScores,2)<size(pScore,2)
            ldScores(1,size(pScore,2))=0;
        end
        if size(pScore,2)<size(ldScores,2)
            pScore(1,size(ldScores,2))=0;
        end
        dShift(j)=mean(sqrt(sum((ldScores-pScore).^2,2)));
        matched(j)=length(find(grpIdG==grpId'))/length(grpId);
        plot(j,matched(j)/dShift(j),'r.');
        if j>1
            if (matched(j)/dShift(j)) < (matched(j-1)/dShift(j-1))
                % if the performance gets worse this component was bad
                scoreSave(j)=0;
                find(scoreSave)
            end
        end
        drawnow;
        hold on;
        j=j+1;
        if j>length(scoreSave) % all the scores used up
            break;
        end
    end
else
    scoreSave(1:(round(min([20 grpCnt/2]))))=1; % use up to the first 20 comps
end
coeffs=coeff(:,logical(scoreSave)); % delete the extra components
scores=score(:,logical(scoreSave)); % delete the extra scores
[grpIdG,pScore]=tycrossval('lda',datatable,grpId,Folds,logical(scoreSave));
f=lda(scores,grpId);
[ldPC,b]=cvar(f);
ldPC=coeffs*ldPC;
ldScores=datatable*ldPC;
grpIdG=classify(f,scores);


