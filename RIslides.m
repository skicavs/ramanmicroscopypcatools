function RIslides(runz,pcaMode)
% This tool is for the live imaging feature of the raman imaging section of
% pcatools
%pcaMode=1;
if nargin<1
    runz=safeLoad;
end
if nargin<2
    pcaMode=0;
end
mainFig=figure;
liData=runz.ramanimage;
if pcaMode==1
    liData.data=runz.statz.score;
end
if iscell(liData.data)
    liData.data=liData.data{1};
end
subplot(3,1,1)
image(liData.raw)
title('White Light Image')
subplot(3,1,2)
imPlot=imagesc(liData.liveimage.xBnds,liData.liveimage.yBnds,zeros(length(liData.liveimage.xBnds),length(liData.liveimage.yBnds)));
imTit=title('Raman Image at cm^{-1}');
%liveBar=colorbar;
aRI=axis;
subplot(3,1,1)
axis(aRI)
liveAxes=subplot(3,1,3);
if pcaMode==1
    y = runz.statz.latent/sum(runz.statz.latent)*100;
    m = length(sprintf('%.0f',length(y)));
    names = reshape(sprintf(['%' int2str(m) '.0f'],1:length(y)),m,length(y))';
    y = y(:);
    [yy,ndx] = sort(y);
    yy = flipud(yy); ndx = flipud(ndx);
    bar('v6',liveAxes,1:length(y),yy);
    hold on;
    line(1:length(y),cumsum(yy));
    ysum = sum(yy);
    k = min(find(cumsum(yy)/ysum>.95,1),10);
    set(liveAxes,'xlim',[.5 k+.5])
    set(liveAxes,'xtick',1:k,'xticklabel',names(ndx,:),'ylim',[0 ysum])
    specTitle=title('Variances explained by the PCs');    %  the first 10 PCs explain
    xlabel('Principal component');
    ylabel('Variance explained (%)');
    xData=1:size(liData.data,2);
    hStar=plot(xData(1),runz.statz.latent(xData(1)),'r+','MarkerSize',10);
    
else
   xData=runz.cellz{1}.relwavenumber; 
plot(runz.cellz{1}.relwavenumber,runz.cellz{1}.avgPlot,'b-')
hold on
hStar=plot(xData(1),runz.cellz{1}.avgPlot(1),'r+','MarkerSize',10);
try
plot(runz.cellz{1}.relwavenumber,runz.cellz{1}.avgPlot+runz.cellz{1}.stdG,'b--');
plot(runz.cellz{1}.relwavenumber,runz.cellz{1}.avgPlot-runz.cellz{1}.stdG,'b--');
catch
    warning('No STD')
end
specTitle=title('Averaged Spectrum : Click To View Image');
xlabel('Wavenumber (cm^{-1})')
ylabel('Normalized Intensity (a.u.)')
end
set(liveAxes,'ButtonDownFcn',@clickAxes);


%mS=num2str(size(liData.liveimage.img,3));
machenimage = uicontrol('Style', 'pushbutton', 'String', 'Make Color Image','Position', [0 0 200 30], 'Callback',@MakeImage);
[xD,yD]=meshgrid(runz.ramanimage.liveimage.yBnds,runz.ramanimage.liveimage.xBnds);
uiwait(mainFig);

    function clickAxes(varargin)
        mPoint=get(liveAxes,'currentpoint');
        [junk cDex]=min(abs(mPoint(1,1)-xData))
        tempImg=zeros(size(liData.liveimage.img,1),size(liData.liveimage.img,2));
        [x,y]=meshgrid(runz.ramanimage.liveimage.yBnds,runz.ramanimage.liveimage.xBnds);
        for i=1:length(liData.impos(:,1))
            if liData.mapping(i)<=size(liData.data,1) & liData.mapping(i)>0
                tempImg=tempImg+liData.data(liData.mapping(i),cDex)*exp(-((xD-liData.impos(i,1)).^2+(yD-liData.impos(i,2)).^2)/(5^2));
            end
        end
        set(imPlot,'CData',tempImg);
        %liveBar=colorbar(liveBar);
        if pcaMode==1
            set(imTit,'String',['Raman Image with PC#' num2str(cDex)]);
        else
        set(imTit,'String',['Raman Image at ' num2str(runz.cellz{1}.relwavenumber(cDex)) 'cm^{-1}']);
        end
        subplot(3,1,3);
        set(hStar,'XData',xData(cDex));
        set(hStar,'YData',runz.cellz{1}.avgPlot(cDex));

    end

    function MakeImage(varargin)
        set(specTitle,'String','Click Wavelength For Rot Image')
        [x(1),y,d]=ginput(1);
        
        set(specTitle,'String','Click Wavelength For Gruener Image')
        [x(2),y,d]=ginput(1);
        set(specTitle,'String','Click Wavelength For Blau Image')
        [x(3),y,d]=ginput(1);
        tempImg=zeros(size(liData.liveimage.img,1),size(liData.liveimage.img,2),3);
        
        for k=1:length(x)
           [junk cDex]=min(abs(x(k)-xData));
           curImg=zeros(size(liData.liveimage.img,1),size(liData.liveimage.img,2));
            
        for i=1:length(liData.impos(:,1))
            if liData.mapping(i)<=size(liData.data,1) & liData.mapping(i)>0
                curImg=curImg+liData.data(liData.mapping(i),cDex)*exp(-((xD-liData.impos(i,1)).^2+(yD-liData.impos(i,2)).^2)/(5^2));
            end
        end
           %curImg=liData.liveimage.img(:,:,cDex);
           curImg=curImg-mean(min(curImg));
           curImg=curImg/mean(max(curImg))*255;
           tempImg(:,:,k)=uint8(curImg);
        end
        tempImg=uint8(tempImg);
        figure
        imshow(tempImg);
        x=round(x);
        title(['R: ' num2str(x(1)) ', G:' num2str(x(2)) ', B:' num2str(x(3))]);
        set(specTitle,'String','Averaged Spectrum : Click To View Image');
    end
        
end

function runz=safeLoad()
uiload
end