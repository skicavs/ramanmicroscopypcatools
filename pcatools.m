function varargout = pcatools(varargin)
% PCATOOLS M-file for pcatools.fig
%      By Kevin Mader (skicavs@bu.edu | kevinmader@gmail.com)
%
%      PCATools is a tool that allows the user to load in spectral data in
%      either ascii or SPE (WinSpec32) format and analyze this spectra
%      using PCA and cluster analysis tools.
%
%      H = PCATOOLS returns the handle to a new PCATOOLS or the handle to
%      the existing singleton*.
%
%      PCATOOLS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PCATOOLS.M with the given input arguments.
%
%      PCATOOLS('Property','Value',...) creates a new PCATOOLS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pcatools_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pcatools_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: RISLIDES, PCACLUSTER

% Edit the above text to modify the response to help pcatools

% Last Modified by GUIDE v2.5 17-Aug-2007 21:56:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @pcatools_OpeningFcn, ...
    'gui_OutputFcn',  @pcatools_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pcatools is made visible.
function pcatools_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pcatools (see VARARGIN)

% Choose default command line output for pcatools
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pcatools wait for user response (see UIRESUME)
% uiwait(handles.figure1);
NoDataLoaded(handles);
NoPCADone(handles);
NoCollapsedDone(handles);
NoClusterDone(handles);
NoImageDone(handles);
pcatoolbar(handles);
PCAVersion='20070818';
disp(['Currently Running PCATools version:' PCAVersion])
disp(['By: Kevin Mader (kevinmader@gmail.com | skicavs@bu.edu)']);

function pcatoolbar(handles)
hTB = findall(handles.figure1,'Tag','FigureToolBar');
if ~isempty(hTB)
    if strcmp(get(hTB,'Visible'),'on');
        return
    end
end
viewmenufcn(handles.figure1,'FigureToolbar');

buttZ=findall(handles.figure1,'Type','uitoggletool','-or','Type','uipushtool');
for k=1:length(buttZ)
    if strcmp(get(buttZ(k),'TooltipString'),'Save Figure')
        set(buttZ(k),'TooltipString','Save Data...');
        set(buttZ(k),'ClickedCallback','pcatools(''fle_Save_Callback'',gcbo,[],guidata(gcbo))');
    end
    if strcmp(get(buttZ(k),'TooltipString'),'Open File')
        set(buttZ(k),'TooltipString','Open Analysis');
        set(buttZ(k),'ClickedCallback','pcatools(''fle_Load_Callback'',gcbo,[],guidata(gcbo))');
    end
    if strcmp(get(buttZ(k),'TooltipString'),'New Figure')
        set(buttZ(k),'TooltipString','New Analysis');
        set(buttZ(k),'ClickedCallback','pcatools(''fle_New_Callback'',gcbo,[],guidata(gcbo))');

    end
    set(buttZ(k),'Visible','on');
end
% --- Outputs from this function are returned to the command line.
function varargout = pcatools_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function exp_Spec_Callback(hObject, eventdata, handles)
% hObject    handle to exp_Spec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
groups={};
for n=1:length(runz.cellz)
    groups{end+1}=runz.cellz{n}.name{1};
end
group=menu('Select Group to Export',groups);

data=[runz.cellz{1}.relwavenumber'];
labels={'RWavenumber'};
for n=1:length(runz.cellz{group}.data)
    data=[data runz.cellz{group}.data{n}.spectrum'];
    labels{end+1}=[runz.cellz{group}.name{1} ':' runz.cellz{group}.data{n}.name];
end
data=[data runz.cellz{1}.avgPlot'];
labels{end+1}='Average Spectra';
[fileN,dlm]=getOutFile();
writeASC(fileN,data,labels,dlm);

% --------------------------------------------------------------------
function exp_Avg_Callback(hObject, eventdata, handles)
% hObject    handle to exp_Avg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.plot_GrpAvg,'Value',1); % let the user see what they are plotting and calc STD
updatePlots(handles);
runz=loadRunz(handles);
labels={'RWavenumber'};
data=[runz.cellz{1}.relwavenumber'];
for n=1:length(runz.cellz)
    data=[data runz.cellz{n}.avgPlot' runz.cellz{n}.stdG'];
    labels{end+1}=[runz.cellz{n}.name{1} ' Average'];
    labels{end+1}=[runz.cellz{n}.name{1} ' STD'];
end
[fileN,dlm]=getOutFile();
writeASC(fileN,data,labels,dlm);

% --------------------------------------------------------------------
function exp_Scores_Callback(hObject, eventdata, handles)
% hObject    handle to exp_Scores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
data=runz.statz.score;
labels={};
for j=1:size(data,2)
    labels{end+1}=['PC' num2str(j) ' score'];
end
[fileN,dlm]=getOutFile();
writeASC(fileN,data,labels,dlm);

% --------------------------------------------------------------------
function exp_Components_Callback(hObject, eventdata, handles)
% hObject    handle to exp_Components (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
labels={'RWavenumber'};
data=[runz.cellz{1}.relwavenumber' runz.statz.pc];
for j=1:size(data,2)
    labels{end+1}=['PC ' num2str(j)];
end
[fileN,dlm]=getOutFile();
writeASC(fileN,data,labels,dlm);

% --------------------------------------------------------------------
function fle_New_Callback(hObject, eventdata, handles)
% hObject    handle to fle_New (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

NoCollapsedDone(handles);
axes(handles.axes1)
hold off;
plot(1,1,'b-');

NoDataLoaded(handles);
NoPCADone(handles);

runz=[];
writeRunz(handles,runz);

% --------------------------------------------------------------------
function fle_Load_Callback(hObject, eventdata, handles)
% hObject    handle to fle_Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[runz,Crunz]=safeLoad;
if ~isempty(runz)
    writeRunz(handles,runz);
    DataLoaded(handles);
    if isfield(runz,'statz')
        PCADone(handles);
        set(handles.plot_PCS,'Value',1);
        runz=updatePCA(runz);
    else
        NoPCADone(handles);
        set(handles.plot_GrpAvg,'Value',1);
    end
    if ~isempty(Crunz)
        CollapsedDone(handles)
        set(handles.useCollapsed,'Value',1);
        if isfield(Crunz,'clusterz')
            ClusterDone(handles);
        else
            NoClusterDone(handles);
        end
        writeRunz(handles,Crunz);
    else
        NoCollapsedDone(handles);
        if isfield(runz,'clusterz')
            ClusterDone(handles);
            if isfield(runz.clusterz,'training')
                %set(handles.cluster_Clear,'Visible','on');
            end
        else
            NoClusterDone(handles);
        end
    end
    if isfield(runz,'ramanimage')
        ImageDone(handles)
    else
        NoImageDone(handles)
    end
    writeRunz(handles,runz);
    updatePlots(handles);
end


% --------------------------------------------------------------------
function fle_Save_Callback(hObject, eventdata, handles)
% hObject    handle to fle_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

curVal=get(handles.useCollapsed,'Value');
set(handles.useCollapsed,'Value',0);
runz=loadRunz(handles);
set(handles.useCollapsed,'Value',1);
Crunz=loadRunz(handles);
set(handles.useCollapsed,'Value',curVal);
uisave

% --------------------------------------------------------------------
function fle_Exit_Callback(hObject, eventdata, handles)
% hObject    handle to fle_Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function data_Import_Callback(hObject, eventdata, handles,isImage)
% hObject    handle to data_Import (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.useCollapsed,'Value',0);
if nargin<4
    isImage=0; % raman imaging mode
end
runz=loadRunz(handles);
if isempty(runz)
    runz.cellz={};
    runz.settings={};
    jk=0;
else
    jk=max(runz.cellz{end}.scrange);
end
dataparams=runz.settings;

if isempty(dataparams)
    dataparams={};
    dataparams.firstRun=1;
    dataparams.doBase=menu('Perform Baseline Subtraction?','Yes','No');
    dataparams.doNorm=menu('Would you like to normalize the data?','Yes','No');
    dataparams.doCent=25;
    dataparams.smoothPts=2;
    dataparams.boxcarPts=1;

    %dataparams.doCent=menu('Perform Peak-Centering','Yes','No');
    %dataparams.smoothPts=menu('How many point median filter would you like to run','0','3','5','7')-1;
end
if dataparams.doCent==1
    dataparams.doCent=20;
end
[data,dataparams,avgPlot,wvx,onamae]=loadgroup(dataparams);
runz.cellz{end+1}.name=onamae;
runz.cellz{end}.data=data;
runz.cellz{end}.avgPlot=avgPlot;
runz.cellz{end}.stdG=std(data2mat(data));
runz.cellz{end}.relwavenumber=wvx;
runz.cellz{end}.scrange=[jk+1:jk+size(data,2)];
jk=jk+size(data,2);
runz.settings=dataparams;
if isImage
    % raman imaging code :: is likely buggy, but whatever
    cPath=runz.cellz{end}.data{1}.name;
    if ispc
        dlDex=find(cPath=='\');
    else
        dlDex=find(cPath=='/');
    end
    cPath=cPath(1:dlDex(end));
    runz.ramanimage.impos=csvread([cPath 'impos.csv']);
    runz.ramanimage.raw=imread([cPath 'raw.jpg']);
    ImageDone(handles);
else
    NoImageDone(handles);
end
writeRunz(handles,runz);
DataLoaded(handles);
if isfield(runz,'statz')
    runz=updatePCA(runz);
    writeRunz(handles,runz);
end

if strcmp(get(handles.useCollapsed,'Visible'),'on')
    set(handles.useCollapsed,'Value',1);
    crunz=loadRunz(handles);
    collapseData(handles,crunz.peaks); % integrate the new peaks
    if isfield(crunz,'statz')
        crunz=updatePCA(crunz);
        writeRunz(handles,crunz);
    end

    set(handles.useCollapsed,'Value',0);
end



set(handles.plot_Ind,'Value',1);
set(handles.plot_GrpList,'Value',length(runz.cellz));
updatePlots(handles);

% --------------------------------------------------------------------
function data_Collapse_Callback(hObject, eventdata, handles)
% hObject    handle to data_Collapse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
collapseData(handles);

% --- Executes on button press in calcPCA.
function calcPCA_Callback(hObject, eventdata, handles)
% hObject    handle to calcPCA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
runz.statz=dopca(runz.cellz);
runz=updatePCA(runz);
writeRunz(handles,runz);
PCADone(handles);
set(handles.plot_PCS,'Value',1);
updatePlots(handles)
set(handles.plot_PCS,'String','PCA Scores');
set(handles.plot_PCP,'String','PCA Components');
% --- Executes on selection change in plot_GrpList.
function plot_GrpList_Callback(hObject, eventdata, handles)
% hObject    handle to plot_GrpList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns plot_GrpList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_GrpList
updatePlots(handles)

% --- Executes during object creation, after setting all properties.
function plot_GrpList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_GrpList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in plot_GrpAvg.
function plot_GrpAvg_Callback(hObject, eventdata, handles)
% hObject    handle to plot_GrpAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_GrpAvg
updatePlots(handles)



% --- Executes on button press in show_Legend.
function show_Legend_Callback(hObject, eventdata, handles)
% hObject    handle to show_Legend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of show_Legend
updatePlots(handles)



% --------------------------------------------------------------------
function PlotDisplay_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to PlotDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
updatePlots(handles)



% --- Executes on button press in NumberPlot.
function NumberPlot_Callback(hObject, eventdata, handles)
% hObject    handle to NumberPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NumberPlot
updatePlots(handles)

% --- Executes on button press in regAvg.
function regAvg_Callback(hObject, eventdata, handles)
% hObject    handle to regAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
ptz=getappdata(handles.figure1,'ptz');
if get(handles.plot_RamanImage,'Value')
    % raman image
    xx=runz.ramanimage.impos(:,1);
    yy=runz.ramanimage.impos(:,2);
    rImode=1;
else
    xx=getappdata(handles.figure1,'xx');
    yy=getappdata(handles.figure1,'yy');
    if ~get(handles.plot_Cluster,'Value')
        set(handles.plot_PCS,'Value',1);
        set(handles.NumberPlot,'Value',0);
    end
    rImode=0;
end
newRun=0;
if isempty(ptz)
    newRun=1;
else
    if ptz<2
        newRun=1;
    else
        diffSpecSub=getappdata(handles.figure1,'diffSpecSub');
        if isempty(diffSpecSub) | length(diffSpecSub)~=length(runz.cellz{1}.relwavenumber)
            newRun=1;
        end
    end
end
if newRun==1
    updatePlots(handles);
    [a,b]=size(runz.cellz{1}.relwavenumber);
    diffSpecSub=zeros(a,b);
end

ptz=getappdata(handles.figure1,'ptz');
axes(handles.axes1);
set(handles.txt_Status,'String','Please click the boundary for the area you would like to average. Right click the last point');
d=1;
xd=[];
yd=[];
hd=[];


avgPlot=zeros(1,length(runz.cellz{1}.relwavenumber));


while d==1
    [x,y,d]=ginput(1);
    xd(end+1)=x;
    yd(end+1)=y;
    if length(xd)>1
        hd(end+1)=line([xd(end-1),xd(end)],[yd(end-1),yd(end)],'Color',[1,0,0]);
    end
end

hd(end+1)=line([xd(1) xd(end)],[yd(1) yd(end)],'Color',[1,0,0]);


cnt=0;
for i=1:length(xx)
    if npoly(xd,yd,xx(i),yy(i))
        if rImode
            j=1;
            k=runz.ramanimage.mapping(i);
        else
            [j,k]=unwind(runz,i);
        end
        avgPlot=avgPlot+runz.cellz{j}.data{k}.spectrum;
        %plot(xx(i),yy(i),'k.');
        cnt=cnt+1;
    end
end
if ptz==1
    dSpec=menu('Would you like to subtract this spectra from the other spectra?','No','Yes')-1;
else
    if sum(abs(diffSpecSub))>0

        dSpec=-1;
    else
        dSpec=0;
    end
end
if dSpec==1
    dSpec=-1;
    diffSpecSub=avgPlot/cnt;
    text(mean(xd),mean(yd),['Reference Spectra']);
    figure(1)
    title('Difference Spectra');

else
    text(mean(xd),mean(yd),['Region ' num2str(ptz+dSpec)]);
    dplot(runz.cellz{1}.relwavenumber,avgPlot/cnt-diffSpecSub,['Region ' num2str(ptz+dSpec)],ptz+dSpec);
end
ptz=ptz+1;
setappdata(handles.figure1,'ptz',ptz);
setappdata(handles.figure1,'diffSpecSub',diffSpecSub);
set(handles.txt_Status,'String','');



% --- Executes on button press in savePlot.
function savePlot_Callback(hObject, eventdata, handles)
% hObject    handle to savePlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname,fltDex] = uiputfile({'*.jpg', 'JPG Image';'*.eps', 'Encapsulated PostScript';'*.pdf', 'PDF'},'Save graph as');
extB=find(filename=='.');
fileN=[pathname filename];
switch fltDex
    case 1
        extC='jpg';
    case 2
        extC='eps';
    case 3
        extC='pdf';
end
if isempty(extB)
    fileN=[fileN '.' extC];
end
saveas(handles.axes1,fileN,extC);


% --- Executes on button press in useCollapsed.
function useCollapsed_Callback(hObject, eventdata, handles)
% hObject    handle to useCollapsed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of useCollapsed
runz=loadRunz(handles);
if isfield(runz,'statz')
    PCADone(handles);
else
    NoPCADone(handles);

end
if isfield(runz,'clusterz')
    ClusterDone(handles);
    if isfield(runz.clusterz,'training')
        set(handles.cluster_Clear,'Visible','on');
    end
else
    NoClusterDone(handles);
end
updatePlots(handles);


% --------------------------------------------------------------------
function cpse_Callback(hObject, eventdata, handles)
% hObject    handle to cpse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cpse_Load_Callback(hObject, eventdata, handles)
% hObject    handle to cpse_Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

runz=loadRunz(handles);
loadOld=1;
[filen,path,filterindex]=uigetfile({'*.csv;*.CSV','Comma Separated Values'},'Multiselect','off');
try
    peaks=dlmread([path filen],',');
catch
    loadOld=0;
    warning('File Loading Failed');
end
if isempty(peaks)
    loadOld=0;
end
if loadOld
    collapseData(handles,peaks);
else
    warning('Load Peak List Aborted!');
end

% --------------------------------------------------------------------
function cpse_Edit_Callback(hObject, eventdata, handles)
% hObject    handle to cpse_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
curVal=get(handles.useCollapsed,'Value');
set(handles.useCollapsed,'Value',1);
runz=loadRunz(handles); % read the smallRunz data
collapseData(handles,runz.peaks,1);
%set(handles.useCollapsed,'Value',curVal);
% --------------------------------------------------------------------
function cpse_Save_Callback(hObject, eventdata, handles)
% hObject    handle to cpse_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
curVal=get(handles.useCollapsed,'Value');
set(handles.useCollapsed,'Value',1);
runz=loadRunz(handles); % read the smallRunz data
try
    [filen,path]=uiputfile({'*.csv;*.CSV','Comma Separated List'});
    dlmwrite([path filen],runz.peaks,',');
catch
    warning('Peak List Could Not Be Writtten');
end
set(handles.useCollapsed,'Value',curVal);

% --------------------------------------------------------------------
function cluster_Run_Callback(hObject, eventdata, handles)
% hObject    handle to cluster_Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
pcs=runz.clusterz.pcs;
grpId=runz.clusterz.grpId;
%% Allow User To Test Clustering
%blindTest=menu('Would you like the program
pcnt=[.05:0.05:1];
%curkPC=1:PCKeep;
ccEl=find(grpId>0);
datamatrix=[];
for k=1:(length(runz.cellz))
    tempMat=data2mat(runz.cellz{k}.data);
    datamatrix=[datamatrix; tempMat];
end
datamatrix=datamatrix-repmat(mean(datamatrix),size(datamatrix,1),1);
[ldPC,ldScores,grpIdG,f] = pcacluster(datamatrix(ccEl,:),grpId(ccEl)); % use only classified components
runz.clusterz.grpIdGuess=grpId;
runz.clusterz.grpIdGuess(ccEl)=grpIdG; % only fill in the classified groups
runz.clusterz.classifier=f;

%plot(pcnt,Mlab,'r-',pcnt,Lscr,'b-',pcnt,Qscr,'g-',pcnt,Logr,'c-',pcnt,Softm,'k-');
%legend({'Matlab:Classify','LDA','QDA','LogisitcDA','Softmax'})
%runz.clusterz.grpIdGuess=runz.clusterz.grpId;
% if this is the training then reset the PCS
% this code sets the principal components to the components that are
% used to discriminate groups make the plot an LDA plot
if size(ldPC,2)<2
    ldPC(:,2)=0; % stop being such a tool
end
runz.statz.pc=ldPC;
[runz,pcs]=updatePCA(runz);
runz.clusterz.pcs=pcs;
runz.clusterz.training.pcs=pcs(ccEl,:);
runz.clusterz.training.grpId=grpId(ccEl);
writeRunz(handles,runz);
set(handles.plot_PCS,'String','LDA Scores');
set(handles.plot_PCP,'String','LDA Components');
set(handles.plot_Cluster,'Value',1);
updatePlots(handles);
%[grpIdGuess]=classify(runz.clusterz.pcs(:,[1:PCKeep]),runz.clusterz.training.pcs(:,[1:PCKeep]),runz.clusterz.training.grpId,'mahalanobis');
%runz.clusterz.grpIdGuess=grpIdGuess;


% --------------------------------------------------------------------
function cluster_Clear_Callback(hObject, eventdata, handles)
% hObject    handle to cluster_Clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
if isfield(runz,'clusterz')
    if isfield(runz.clusterz,'training')
        runz.clusterz=rmfield(runz.clusterz,'training');
        writeRunz(handles,runz);
    end
end

%set(handles.cluster_Clear,'Visible','off');

% --------------------------------------------------------------------
function cluster_Identify_Callback(hObject, eventdata, handles)
% hObject    handle to cluster_Identify (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

createGroups(handles,1)



% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mPoint=get(hObject,'currentpoint');
if get(handles.plot_PCS,'Value') | get(handles.plot_Cluster,'Value') | get(handles.plot_RamanImage,'Value')
    if get(handles.dispPoints,'Value')
        pointClick(handles,mPoint(1,1),mPoint(1,2),1);
    elseif get(handles.delPoints,'Value')
        pointClick(handles,mPoint(1,1),mPoint(1,2),3);
    end
end

% --- Executes on button press in liveRaman.
function liveRaman_Callback(hObject, eventdata, handles)
% hObject    handle to liveRaman (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
liveImage(handles);


% --------------------------------------------------------------------
function data_Image_Callback(hObject, eventdata, handles)
% hObject    handle to data_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data_Import_Callback(hObject,eventdata,handles,1); % load image data
runz=RImakemap(runz);



% --------------------------------------------------------------------
function cluster_Blind_Callback(hObject, eventdata, handles)
% hObject    handle to cluster_Blind (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% This function uses different techniques to blindly identify groups
cluster_Clear_Callback(hObject, eventdata, handles); % clear old training

runz=loadRunz(handles);
seedData=menu('Select Data to Use for Analysis:','Raw Spectral Data','PCA Scores');
if seedData==0
    return;
end
if seedData==1
    datamatrix=[];
    for k=1:(length(runz.cellz))
        tempMat=data2mat(runz.cellz{k}.data);
        datamatrix=[datamatrix; tempMat];
    end
    datamatrix=datamatrix-repmat(mean(datamatrix),size(datamatrix,1),1);
    cData=datamatrix;
else
    [runz,pcs]=updatePCA(runz);
    runz.clusterz.pcs=pcs;
    cData=pcs;
end
method=menu('Select Method to Use for Analysis:','K-Means','Mean Shift');
switch method
    case 1
        % K-Means
        grps=inputdlg('How many groups do you expect/want?','Groups',1,{'2'});
        if isempty(grps)
            return;
        end
        grps=str2num(grps{1});
        if grps<2
            return;
        end
        runz.clusterz.grpId=kmeans(cData,grps);
    case 2
        % Mean Shift
        sqDistToAll = sum((repmat(mean(cData)',1,size(cData,1)) - cData').^2);
        sDist=median(sqrt(sqDistToAll));
        tol=inputdlg('Enter Tolerance for Group Seperation','Tolerance',1,{num2str(sDist)});
        if isempty(tol)
            return;
        end
        tol=str2num(tol{1});
        if tol<0
            return;
        end
        [junk,runz.clusterz.grpId,junk] = MeanShiftCluster(cData',tol,false);
        disp(['Number of Groups:' num2str(max(runz.clusterz.grpId))]);

end
writeRunz(handles,runz);
ClusterDone(handles)
set(handles.plot_Cluster,'Value',1)
updatePlots(handles);

%[ldPC,ldScores,grpIdG] = pcacluster(datamatrix(ccEl,:),grpId(ccEl));

% --------------------------------------------------------------------
function data_Del_Callback(hObject, eventdata, handles)
% hObject    handle to data_Del (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
runz=loadRunz(handles);
optList={'None'};
for i=1:length(runz.cellz)
    optList{i+1}=runz.cellz{i}.name{1};
end
toDel=menu('Which Data Set To Delete?',optList)-1;
if toDel>1
    deleteFileGroup(handles,toDel);
elseif toDel==1 & length(runz.cellz)==1
    % last group
    fle_New_Callback(hObject,eventdata,handles);
elseif toDel==1
    deleteFileGroup(handles,toDel);
end


%% These functions handle making things visible and invisible at
%% appropriate times
function DataLoaded(handles)
set(handles.PlotDisplay,'Visible','on')
set(handles.axes1,'Visible','on')
set(handles.data_Collapse,'Visible','on')
runz=loadRunz(handles);
curL={};
for ij=1:length(runz.cellz)
    curL{ij}=runz.cellz{ij}.name{1};
end
set(handles.plot_GrpList,'String',curL);
set(handles.calcPCA,'Visible','on');
set(handles.savePlot,'Visible','on');
set(handles.txt_Status,'String','');
set(handles.cpse,'Visible','on');
set(handles.data_Image,'Visible','off');
set(handles.data_Del,'Visible','on');
function NoDataLoaded(handles)
set(handles.PlotDisplay,'Visible','off')
set(handles.axes1,'Visible','off')
set(handles.data_Collapse,'Visible','off')
set(handles.calcPCA,'Visible','off');
set(handles.savePlot,'Visible','off');
set(handles.txt_Status,'String','Please Load or Import Data Before Continuing');
set(handles.cpse,'Visible','off');
set(handles.data_Image,'Visible','on');
set(handles.data_Del,'Visible','off');

function PCADone(handles)
set(handles.fle_Export,'Visible','on');
set(handles.plot_PCS,'Visible','on');
set(handles.plot_PCP,'Visible','on');
set(handles.plot_Var,'Visible','on');
set(handles.NumberPlot,'Visible','on');
set(handles.regAvg,'Visible','on');
set(handles.dispDel,'Visible','on');
set(handles.cluster,'Visible','on');
function NoPCADone(handles)
set(handles.fle_Export,'Visible','off')
set(handles.plot_PCS,'Visible','off');
set(handles.plot_PCP,'Visible','off');
set(handles.plot_Var,'Visible','off');
set(handles.NumberPlot,'Visible','off');
set(handles.regAvg,'Visible','off');
set(handles.dispDel,'Visible','off');
swT=get(handles.plot_PCS,'Value') | get(handles.plot_PCP,'Value') | get(handles.plot_Var,'Value');
if swT
    set(handles.plot_GrpAvg,'Value',1);
    updatePlots(handles);
end
set(handles.cluster,'Visible','off');
function CollapsedDone(handles)
set(handles.useCollapsed,'Visible','on');
set(handles.cpse_Edit,'Visible','on');
set(handles.cpse_Save,'Visible','on');
function NoCollapsedDone(handles)
set(handles.useCollapsed,'Visible','off');
set(handles.useCollapsed,'Value',0);
set(handles.cpse_Edit,'Visible','off');
set(handles.cpse_Save,'Visible','off');

function ClusterDone(handles)
set(handles.cluster_Run,'Visible','on');
set(handles.plot_Cluster,'Visible','on');

function NoClusterDone(handles)
set(handles.cluster_Run,'Visible','off');
if get(handles.plot_Cluster,'Value')
    set(handles.plot_GrpAvg,'Value',1);
    updatePlots(handles);
end
set(handles.plot_Cluster,'Visible','off');



function ImageDone(handles)
set(handles.plot_RamanImage,'Visible','on');
set(handles.liveRaman,'Visible','on');
set(handles.data_Import,'Visible','off');
set(handles.data_Image,'Visible','off');
function NoImageDone(handles)
set(handles.plot_RamanImage,'Visible','off');
set(handles.liveRaman,'Visible','off');
set(handles.data_Import,'Visible','on');
%% The beginning of the real code for the program
function updatePlots(handles)
% This function updates the plot in the main window
runz=loadRunz(handles);
clz='brgky';
ptsz='o.+p*^';
axes(handles.axes1);
hold off;
legText={};
setappdata(handles.figure1,'ptz',1);
if get(handles.plot_Ind,'Value')
    % read current group
    % plot a group worth of data
    curGrp=get(handles.plot_GrpList,'Value');

    for jk=1:length(runz.cellz{curGrp}.data)
        curPlot=plot(runz.cellz{curGrp}.relwavenumber,runz.cellz{curGrp}.data{jk}.spectrum,[clz(mod(jk-1,length(clz))+1) '-']);
        hold on;
        legText{jk}=runz.cellz{curGrp}.data{jk}.name;
    end
    xlabel('Wavenumber cm^{-1}');
    ylabel('Intensity (a.u.)');
end
if get(handles.plot_GrpAvg,'Value')
    % plot group averages
    for jk=1:length(runz.cellz)
        if ~isfield(runz.cellz{jk},'stdG')
            runz.cellz{jk}.stdG=std(data2mat(runz.cellz{jk}.data));
            writeRunz(handles,runz);
        end
        plot(runz.cellz{jk}.relwavenumber,runz.cellz{jk}.avgPlot,[clz(mod(jk-1,length(clz))+1) '-']);
        %errorbar(runz.cellz{jk}.relwavenumber,runz.cellz{jk}.avgPlot,runz.cellz{jk}.stdG,[clz(mod(jk-1,length(clz))+1) '-']);

        hold on;
        legText{jk}=runz.cellz{jk}.name{1};
    end
    %for jk=1:length(runz.cellz)
        %plot(runz.cellz{jk}.relwavenumber,runz.cellz{jk}.avgPlot+runz.cellz{jk}.stdG,[clz(mod(jk-1,length(clz))+1) '--']);
        %plot(runz.cellz{jk}.relwavenumber,runz.cellz{jk}.avgPlot-+runz.cellz{jk}.stdG,[clz(mod(jk-1,length(clz))+1) '--']);
    %end
    xlabel('Wavenumber cm^{-1}');
    ylabel('Intensity (a.u.)');
end

if get(handles.plot_Var,'Value')
    %pareto(handles.axes1,runz.statz.latent/sum(runz.statz.latent)*100);    %  visualize how much variance
    y = runz.statz.latent/sum(runz.statz.latent)*100;
    m = length(sprintf('%.0f',length(y)));
    names = reshape(sprintf(['%' int2str(m) '.0f'],1:length(y)),m,length(y))';
    y = y(:);
    [yy,ndx] = sort(y);
    yy = flipud(yy); ndx = flipud(ndx);
    bar('v6',handles.axes1,1:length(y),yy);
    hold on;
    line(1:length(y),cumsum(yy));
    ysum = sum(yy);
    k = min(find(cumsum(yy)/ysum>.95,1),10);
    set(handles.axes1,'xlim',[.5 k+.5])
    set(handles.axes1,'xtick',1:k,'xticklabel',names(ndx,:),'ylim',[0 ysum])
    title('Variances explained by the PCs');    %  the first 10 PCs explain
    xlabel('Principal component');
    ylabel('Variance explained (%)');
end
if get(handles.plot_PCS,'Value')

    %runz=updatePCA(runz);
    writeRunz(handles,runz);
    xx=[];
    yy=[];
    textColors=[[0 0 1];[1 0 0];[0 1 0];[0 0 0];[1 1 0];];%;[.5 0 .5];[0 1 1];[1 1 0]];
    for j=1:length(runz.cellz)

        xx=[xx runz.statz.score{j}(:,1)'];
        if size(runz.statz.score{j},2)<2

        else
            yy=[yy runz.statz.score{j}(:,2)'];
        end
        if size(runz.statz.score{j},2)<2
            plot(runz.statz.score{j}(:,1),zeros(1,size(runz.statz.score{j},1)),[clz(mod(j-1,length(clz))+1) ptsz(mod(j-1,length(ptsz))+1)]);
        else
            plot(runz.statz.score{j}(:,1),runz.statz.score{j}(:,2),[clz(mod(j-1,length(clz))+1) ptsz(mod(j-1,length(ptsz))+1)]);
        end
        hold on;
        legText{j}=runz.cellz{j}.name{1};
    end

    grid on;
    if get(handles.NumberPlot,'Value') %if the numbers should be plotted as well
        datan=axis; % the range of all the points
        offsetN=0.005*(datan(2)-datan(1)); % the data will be moved over .5% of the range from each point
        for j=1:length(runz.cellz)

            for jk=1:size(runz.statz.score{j},1)
                text(runz.statz.score{j}(jk,1)+offsetN,runz.statz.score{j}(jk,2),num2str(jk),'Color',textColors(mod(j-1,length(clz))+1,:)) % plot the numbers
            end
        end
    end
    title('Principle Component Analysis');
    xlabel('1st principal component');
    ylabel('2nd principal component');
    setappdata(handles.figure1,'xx',xx);
    setappdata(handles.figure1,'yy',yy);
    set(handles.dispDel,'Visible','on');
else
    set(handles.dispDel,'Visible','off');
end
if get(handles.plot_PCP,'Value')
    for j=1:min([3,size(runz.statz.pc,2)])
        curComp=(runz.statz.pc(:,j));
        plot(runz.cellz{1}.relwavenumber,abs(curComp),[clz(mod(j-1,length(clz))+1) '-']);
        hold on;
        legText{j}=['Principal Component #' num2str(j)];
    end
    xlabel('Wavenumber cm^{-1}');
    ylabel('Strength of Contribution');
end
if get(handles.plot_Cluster,'Value')
    pcs=runz.clusterz.pcs;
    %rmfield(runz,'jubs');

    %grpId=runz.clusterz.grpId;
    if isfield(runz.clusterz,'grpId')
        grpId=runz.clusterz.grpId;
    else
        grpId=ones(1,size(pcs,1));
    end
    if isfield(runz.clusterz,'training')
        try
            [grpIdGuess]=classify(runz.clusterz.pcs,runz.clusterz.training.pcs,runz.clusterz.training.grpId,'mahalanobis');
            runz.clusterz.grpIdGuess=grpIdGuess;
        catch
            grpIdGuess=[];
        end
        % guess the classifications based on training data
    else
        grpIdGuess=[];
    end
    clz='rgbmcy';
    ptsz='......+++++****';
    legText={};
    for j=1:max(grpId)
        cScs=find(grpId==j);
        if ~isempty(cScs)
            cscore=pcs(cScs,:); % get the scores for the current group
            if size(cscore,2)<2
                plot(cscore(:,1),zeros(1,length(cscore)),[clz(mod(j-1,6)+1) ptsz(mod(j-1,6)+1)]);
            else

                plot(cscore(:,1),cscore(:,2),[clz(mod(j-1,6)+1) ptsz(mod(j-1,6)+1)]);
            end
            hold on
            legText{end+1}=['Cluster Group #' num2str(j)];
        end
    end
    cscore=pcs(find(grpId==0),:); % get the scores for the current group
    if ~isempty(cscore)
        
        if size(cscore,2)<2
                plot(cscore(:,1),zeros(1,length(cscore)),'k.');
            else

                plot(cscore(:,1),cscore(:,2),'k.');
            end
        
        legText{end+1}=['Unclassified : ' num2str(round(length(cscore)/length(grpId)*100)) '%'];
        hold on
    end

    grid on;
    hold on;
    if isfield(runz.clusterz,'training')
        tPCS=runz.clusterz.training.pcs(:,[1:min([2 size(runz.clusterz.training.pcs,2)])]);
        tGrpId=runz.clusterz.training.grpId;
    else
        tPCS=runz.clusterz.pcs(:,[1:min([2 size(runz.clusterz.pcs,2)])]);
        tGrpId=grpId;
    end
    % Plot Decision Regions code stolen from plotdr
    a = axis;
    nint = 256;
    ccEl=find(tGrpId>0);
    x = linspace(a(1), a(2), nint);
    y = linspace(a(3), a(4), nint);
    [X Y] = meshgrid(x, y);
    fakeScoreMat=[X(:) Y(:)];
    while size(fakeScoreMat,2)<size(tPCS,2)
        fakeScoreMat(:,end+1)=mean(tPCS(:,size(fakeScoreMat,2)+1))*ones(size(fakeScoreMat,1),1);
        % make the other dimensions equal to the average of those
    end
    if size(fakeScoreMat,2)>size(tPCS,2)
        fakeScoreMat(:,size(tPCS,2)+1:end)=[];
    end
    if ~isfield(runz.clusterz,'training')
        if max(tGrpId)>1
            
            f=lda(tPCS(ccEl,:),tGrpId(ccEl));
            [c post] = classify(f, fakeScoreMat);
            if isempty(grpIdGuess)
                grpIdGuess=classify(f,tPCS); % guess the groups

            end
        else
            c=ones(size(fakeScoreMat,1),1);
            post=[];
        end
    else
        [c post]=classify(fakeScoreMat,tPCS(ccEl,:),tGrpId(ccEl),'mahalanobis');
        post=[];
    end
    % pause on the decision region codes to plot the group guesses
    if ~isempty(grpIdGuess)
        legText={};
        [sens,spec]=SensAndSpec(grpId,grpIdGuess);
        iClass=0;
        dcsReg=[];
        for j=1:length(grpIdGuess)
            if grpId(j)~=grpIdGuess(j)
                if size(pcs,2)>1
                    dcsReg=[dcsReg;[pcs(j,1),pcs(j,2)]];
                else
                    dcsReg=[dcsReg;[pcs(j,1)]];
                end
                %plot(pcs(j,1),pcs(j,2),'ko','MarkerSize',10);
                iClass=iClass+1;
            end
        end

        for k=1:max(grpId)
            legText{k}=['Group ' num2str(k) ' : Sens-' num2str(round(sens(k)*100)) '% : Spec-' num2str(round(spec(k)*100)) '%'];
        end
        if ~isempty(cscore)
            legText{end+1}=['Unclassified : ' num2str(round(length(cscore)/length(grpId)*100)) '%'];
        end
        if size(dcsReg,1)>0
            if size(dcsReg,2)>1
                plot(dcsReg(:,1),dcsReg(:,2),'ko','MarkerSize',10);
            else
                plot(dcsReg(:,1),zeros(1,length(dcsReg)),'ko','MarkerSize',10);
            end
            legText{end+1}=['Incorrectly Classified : ' num2str(round(iClass/length(grpId)*100)) '%'];
        end
    end
    %for j=1:max(grpId)
    %    cscore=pcs(find(grpId==j),:); % get the scores for the current group
    %    plot(cscore(:,1),cscore(:,2),[clz(mod(j-1,4)+1) ptsz(mod(j-1,4)+1)]);
    %    hold on
    %    legText{end+1}=['Cluster Group #' num2str(j)];
    %end



    % Decision Region Code Finished
    g=max(c);
    p=size(tPCS,2);


    for k = 1:g
        if ~isempty(post)
            s = reshape(max(post(:, [1:k-1 k+1:g]), [], 2) - post(:,k), ...
                repmat(nint, 1, p));
            s=s<0;
        else
            s=reshape(c,repmat(nint,1,p))~=k;

        end
        [ks,maxEl]=bwlabel(bwareaopen(s,10));
        for i=1:maxEl
            [kX,kY]=find(ks==i);
            text(x(round(mean(kY))),y(round(mean(kX))),['Group #' num2str(k)]);
        end
        contour(x, y, s, [0 0], clz(mod(k-1,6)+1));
        %nn=figure
        %imshow(s)
        hold on
    end
    legText{end+1}='Decision Regions';



    title('Group Separation Plot Based On Analysis');
    xlabel('1st principal component');
    ylabel('2nd principal component');
    set(handles.dispDel,'Visible','on');
end

if get(handles.plot_RamanImage,'Value')
    % the special raman image plot mode
    if ~isfield(runz.ramanimage,'mapping')
        runz=RImakemap(runz);
        writeRunz(handles,runz);
    end
        mapping=runz.ramanimage.mapping;
    

    if isfield(runz,'clusterz')
        if isfield(runz.clusterz,'grpId')
            grpId=runz.clusterz.grpId;
        else
            grpId=ones(1,length(runz.cellz{1}.data));
        end
    else
        grpId=ones(1,length(runz.cellz{1}.data));
    end
    imshow(runz.ramanimage.raw);
    hold on;
    clz='rgbmcy';
    ptsz='......+++++****';
    legText={};
    for i=1:size(runz.ramanimage.impos,1)
        if mapping(i)>0
            j=grpId(mapping(i));
        else
            j=1;
        end
        plot(runz.ramanimage.impos(i,1),runz.ramanimage.impos(i,2),[clz(mod(j-1,6)+1) ptsz(mod(j-1,6)+1)]);
        hold on
        %legText{end+1}=['Cluster Group #' num2str(j)];
    end
    grid on;
    hold on;
    set(handles.dispDel,'Visible','on');
end
if ~isempty(legText) & get(handles.show_Legend,'Value')
    legend(legText,'Interpreter','none');
end
set(handles.axes1,'ButtonDownFcn','pcatools(''axes1_ButtonDownFcn'',gcbo,[],guidata(gcbo))');
pcatoolbar(handles);





function runz=RImakemap(runz)
% map the images mapping(imposNumber)=runz struct number
        badMap=[];
        mapping=zeros(size(runz.ramanimage.impos,1),1);
        %pat='(\d+)[^\\]*?\.spe'; % the filename code
        pat='(\d+)\.spe';
        for i=1:length(runz.cellz{1}.data) % raman imaging is only on 1 dataset at a time
            curNum=regexp(lower(runz.cellz{1}.data{i}.name), pat, 'tokens');
            curNum=str2num(curNum{1}{1});
            if ~isempty(curNum)
                mapping(curNum)=i;
            else
                badMap(end+1)=i;
            end
        end
        
            disp('Fixing The Mapping :: Or Trying To');
            eMapS=find(mapping==0);
            mapping(eMapS)=[];
            runz.ramanimage.impos(eMapS,:)=[];
            %while length(eMap) & length(badMap)
            %    mapping(eMapS(end))=badMap(end);
            %    eMapS(end)=[];
            %    badMap(end)=[];
            %end
        
        runz.ramanimage.mapping=mapping;






function [data,dataparams,avgPlot,wvx,onamae]=loadgroup(dataparams)
% loadgroup loads in a bunch of spe files into a data structure
% normVal is the value for normalization either an area or a peak height
% pkc is the location of the peak to use (-1 means use area)
% smpts is the number of points on each to use for the median filter 0-no
% smoothing

if isfield(dataparams,'lastDir')
    lastDir=dataparams.lastDir;
else
    lastDir='*';
end
%[filen,path,filterindex]=uigetfile({'*.spe;*.SPE','WinSpec32 File';'*.txt;*.TXT','Tab Delimited Ascii Text'},'Multiselect','on');
[files,dataparams]=uipickfiles('REFilter','\.spe$|\.txt$|\.SPE$|\.TXT$','Prompt','Select Spectral files to load:','FilterSpec',lastDir,'Output','cell','dataparams',dataparams);

if ischar(files)
    files={files};
end
if ~isempty(files)
    cPath=files{1};
    if ispc
        dlDex=find(cPath=='\');
    else
        dlDex=find(cPath=='/');
    end
    cPath=cPath(1:dlDex(end));
    dataparams.lastDir=cPath;
end
onamae=inputdlg(['Enter group name']);
data={};
avgPlot=0;
hWait=waitbar(0,'Loading spectra files...');
sTime=now;
curWvxA=[];

for jb=1:length(files)
    lChar=upper(files{jb}(end));
    if lChar=='E' %spe file
        [raw,wvxa,wvya]=speread([files{jb}],dataparams.smoothPts,dataparams.boxcarPts);
    elseif lChar=='T' | lChar=='V' % txt file or csv
        [raw,wvxa,wvya]=ascread([files{jb}],dataparams.smoothPts,dataparams.boxcarPts);
    end
    if isempty(curWvxA)
        curWvxA=wvxa;
    end

    curData{jb}.x=wvxa;
    curData{jb}.y=wvya;
    %if
    %if prod(wvxa==curWvxA)==0
        
        wvya=interp1(wvxa,wvya,curWvxA,'cubic',0);
        %wvxa=curWvxA;
     %   warn('Data Does Not Match Dimensionally, Interpolating');
    %end
    avgPlot=avgPlot+wvya;
    timePerSpec=(now-sTime)/jb;

    if (dataparams.firstRun==1)
        timeRemaining=timePerSpec*(length(files)-jb);
        waitbar(jb/(length(files)),hWait,['Do Not Leave : You need to enter setting at: ' datestr(now+timeRemaining,14) '']);
    else
        timeRemaining=timePerSpec*(3*length(files)-jb);
        waitbar(jb/(2*length(files)),hWait,['Loading spectra files (Done at: ' datestr(now+timeRemaining,14) ')']);
    end
end

wvxa=curData{1}.x;
avgPlot=avgPlot/length(files);
if dataparams.firstRun==1 % the initializing statement to get all the parameters correct
    h=figure
    plot(curWvxA,avgPlot);
    uiwait(msgbox('Enter range for data:'));
    stx=cell2mat(inputdlg('Enter wavelength range in standard format (800:1700 means from 800 to 1700, 750:1000 1200:1700 means from 750 to 1700 without 1000 to 1200','Data Range',2,{'750:1000 1200:1700'}));
    dataparams.wavenRange=round(eval(['[' stx ']']));
    close(h)

    crng=interp1(curData{jb}.x,1:length(curData{jb}.x),dataparams.wavenRange,'nearest');

    if dataparams.doCent>9
        h=figure
        plot(curData{jb}.x(crng),avgPlot(crng));
        uiwait(msgbox('Select Calibration Peak(s) to use by clicking'));
        figure(h); % the program gets confused unless you force it to select the current figure
        morePeaks=1;
        centPeak=1;
        while morePeaks
            [x y]=ginput(1);
            dataparams.pkCenter{centPeak}=x;
            [junk leftBound]=min(abs(curData{jb}.x-(x-20)));
            [junk rightBound]=min(abs(curData{jb}.x-(x+20)));
            dataparams.pkData{centPeak}=avgPlot(leftBound:rightBound);
            centPeak=centPeak+1;
            morePeaks=menu('Use Additional Peaks?','No','Yes')-1;
        end
        close(h);
    end
    dataparams.firstRun=0;
else
    if dataparams.doCent>9
        if length(dataparams.pkCenter)==1
            shft=spshift(avgPlot,dataparams.pkData{1},dataparams.pkCenter{1},dataparams.doCent)
            xStretch=interp1([1,length(wvxa)]+shft,[1,length(wvxa)],1:length(wvxa),'cubic','extrap');
        else
            % multiple shifting code to correct poorly calibrated
            % setups
            xft=[];
            shft=[];
            for ipt=1:length(dataparams.pkCenter)
                curPoint=interp1(curData{1}.x,1:length(curData{1}.x),dataparams.pkCenter{ipt},'linear','extrap');
                shft(ipt)=spshift(avgPlot,dataparams.pkData{ipt},curPoint,dataparams.doCent);
                xft(ipt)=curPoint;
            end
            shft
            xStretch=interp1(xft+shft,xft,1:length(wvxa),'cubic','extrap');

            %corWvxA=interp1(xStretch,wvxa,'cubic','extrap');


        end
        %wvxa=interp1(xStretch,wvxa,[1:length(wvxa)],'cubic','extrap');
        wvxa=interp1([1:length(wvxa)],wvxa,xStretch,'cubic','extrap'); % try pleek
    else
        xStretch=1:length(wvxa);
    end
end
sTime=now;
crng=interp1(wvxa,1:length(wvxa),dataparams.wavenRange,'linear');
wvx=interp1(1:length(wvxa),wvxa,crng,'pchip','extrap',0);
avgPlot=0;
for jb=1:length(files)
    wvxa=curData{jb}.x;
    wvya=curData{jb}.y;
    %wvx=interp1(1:length(wvxa),wvxa,crng,'pchip','extrap',0);
    wvy=interp1(1:length(wvya),wvya,crng,'pchip','extrap',0);
    data{jb}.spectrum=wvy;
    data{jb}.name=files{jb};
    if dataparams.doBase==1
        data{jb}.spectrum=data{jb}.spectrum-lieberfake(data{jb}.spectrum)';
    end
    if dataparams.doNorm==1
        dataparams.normVal=1000000;
        data{jb}.spectrum=data{jb}.spectrum*dataparams.normVal/trapz(data{jb}.spectrum);
    end
    avgPlot=avgPlot+data{jb}.spectrum;
    timePerSpec=(now-sTime)/jb;
    timeRemaining=timePerSpec*(length(files)-jb);
    waitbar((length(files)+jb)/(2*length(files)),hWait,['Loading spectra files (Done at: ' datestr(now+timeRemaining,14) ')']);
end
close(hWait);
avgPlot=avgPlot/length(files);
%
%=========================================================================================================================
%
%
function shft=spshift(wvy,pkgrab,pkc,shiftWindow)
% this function slides the data over for a 40pt range to find where the two
% sets match up best
stM=round((length(pkgrab)-1)/2); % left side of peak
stE=stM+(length(pkgrab)-length(-stM:stM)); % right side of peak

shftmat=[-shiftWindow:shiftWindow];
dsq=[];
for j=shftmat
    cdat=interp1([1:length(wvy)],wvy,[pkc+j-stM:pkc+j+stE],'nearest',0);
    cdat=cdat*trapz(pkgrab)/trapz(cdat); % scale the data to the area
    dsq(end+1)=sum((pkgrab-cdat).^2);
end

[junk shft]=min(dsq);
shft=shftmat(shft);

function [runz,Crunz]=safeLoad
uiload;
if ~exist('runz','var')
    runz=[];
end
if ~exist('Crunz','var')
    Crunz=[];
end

%=========================================================================================================================
%
%
function statz=dopca(cellz)
%
%
supervised='';
baseStr='[pc,score,latent,tsquare] = kpca(';

for k=1:(length(cellz)-1)
    baseStr=[baseStr 'data2mat(cellz{' num2str(k) '}.data),''' cellz{k}.name{1} ''',' ];
end

baseStr=[baseStr 'data2mat(cellz{' num2str(length(cellz)) '}.data),''' cellz{end}.name{1} '''' supervised ');' ];
eval(baseStr);

statz.pc=pc;
for j=1:length(cellz)
    statz.score{j}=score([rewind(cellz,j,1):rewind(cellz,j,length(cellz{j}.data))],:);
end
statz.latent=latent;
statz.tsquare=tsquare;

%
%==========
function [runz,score]=updatePCA(runz)
% this function calculates the scores based on the current pca components
%
datamatrix=[];
for k=1:(length(runz.cellz))
    tempMat=data2mat(runz.cellz{k}.data);
    datamatrix=[datamatrix; tempMat];
end
datamatrix=datamatrix-repmat(mean(datamatrix),size(datamatrix,1),1);
score=datamatrix*runz.statz.pc;
if ~iscell(runz.statz.score)
    runz.statz.score={};
end
for j=1:length(runz.cellz)
    runz.statz.score{j}=score([rewind(runz.cellz,j,1):rewind(runz.cellz,j,length(runz.cellz{j}.data))],:);
end
if isfield(runz,'clusterz')
    runz.clusterz.pcs=score;
end
%
%=========================================================================================================================
%
%
function amat=data2mat(data)
%this function converts my data matrix into an array
amat=[];
for k=1:length(data)
    hdata=data{k}.spectrum;
    if size(hdata,1)~=1
        hdata=hdata';
    end
    amat=[amat;hdata;];
end








function [pc,score,latent,tsquare]=kpca(varargin)
%kpca is basically the same as the pca tool written by the other summer
%intern except you can do as many groups as you would like, and the code is
%about half the length

%
% Build the data matrix and perform the PCA analysis
%
runStrs='[';

for j=1:(length(varargin)/2)  %varargin is a variable number of input arguments
    data{j}=varargin{2*j-1};  %assign each data structure to a group of spectra
    runStrs=[runStrs 'data{' num2str(j) '}; '];
end

eval(['datamatrix=' runStrs '];']);
[pc, score, latent, tsquare] = princomp(datamatrix);
%
% Set the sign of each eigenvector by using the convention that the first
% element of each eigenvector is positive
%
for i=1:size(pc,2)
    if pc(1,i)<0; pc(:,i)=-pc(:,i); end
end

datamatrix=datamatrix-repmat(mean(datamatrix),size(datamatrix,1),1);
score=datamatrix*pc;


function [fileN,dlm]=getOutFile()
% gets the name and file type of the file to be written to by the user
[filename, pathname,fltDex] = uiputfile({'*.txt', 'Tab Delimited Text';'*.csv', 'Comma Delmited Text';'*.*', 'Comma Delimited Other'},'Save output as');
extB=find(filename=='.');

fileN=[pathname filename];
if fltDex>1
    dlm=',';
    extC='.csv';
else
    dlm='\t';
    extC='.txt';
end
if isempty(extB)
    fileN=[fileN extC];
end

function writeASC(fileN,data,labels,dlm)
%writes an ascii fileN in file with column headers label and column contents data
f=fopen(fileN,'w');
% write the header
hString=[labels{1}];
for k=2:length(labels)
    hString=[hString dlm labels{k}];
end
fprintf(f,[hString '\n']);
% write the data
for j=1:size(data,1)
    dString=[num2str(data(j,1))];
    for k=2:size(data,2)
        dString=[dString dlm num2str(data(j,k))];
    end
    fprintf(f,[dString '\n']);
end
fclose(f);



function c=npoly(xp,yp,x,y)
% this function determines if the point x,y is within the polygon defined
% by xp, yp it is a bit of mathematical magic
npol=length(xp);
i=1;
j=npol;
c=0;
while i<=npol
    if ((((yp(i)<=y) & (y<yp(j))) | ((yp(j)<=y) & (y<yp(i)))) & (x < (xp(j) - xp(i)) * (y - yp(i)) / (yp(j) - yp(i)) + xp(i)))
        c=~c;
    end
    j=i;
    i=i+1;
end

function [j,k]=unwind(runz,cpoint)
% this function maps a point in the huge score matrix to a cell(j) and a
% spectrum (k)
j=1;
cCount=0;
while cCount<cpoint
    if cpoint<=(cCount+length(runz.cellz{j}.data))
        k=cpoint-cCount;
        cCount=cpoint;
    else
        cCount=cCount+length(runz.cellz{j}.data);
        j=j+1;
    end
end
function cpoint=rewind(cellz,j,k)
% this function ist the inverse of the unwind function
sJ=1;
sK=1;
cpoint=0;
while sJ<j
    cpoint=cpoint+length(cellz{sJ}.data);
    sJ=sJ+1;
end
cpoint=cpoint+k;

function dplot(x,y,name,ptz)
clz='rgbmkcy';
figure(1)
hold on;
plot(x,y,[clz(mod(ptz-1,4)+1) '-'])
pause(.1)
[h,objh,outh,outm]=legend;
outm{end+1}=name;
legend(outm,'Interpreter','none');


function [b,wvx,wvy]=speread(filename,smpts,bcPts);
%SPERead Function
% By Kevin Mader (2007)
% Email (kevinmader@gmail.com)
%About
% Designed to read the WinSpec Files (.spe) into matlab and prepare them
% for analysis using the PCAnalysis tool
%Operation
% [rawData,wvx,wvy]=speread(filename,smpts)
%INPUT Arguments
% filename- is the name and path of the file you wish to load
% smpts- is the number os points to use for the median filter, default 0
%OUTPUT Arguments
% rawData- is the raw spectra from the file
% wvx- is the wavenumbers sampled at 1 point per cm^-1
% wvy- is the raw spectra resampled to match wvx
if nargin<2
    smpts=0;
end
k=fopen(filename,'r','ieee-le');
fseek(k,42,-1);
xdim=fread(k,1,'short')
fseek(k,656,-1);
ydim=fread(k,1,'short')
fseek(k,108,-1);
datatype=fread(k,1,'short')
switch datatype
    case 0
        datatype='float';
    case 1
        datatype='long';
    case 2
        datatype='short';
    case 3
        datatype='ushort';
end
fseek(k,3101,-1);
polyorder=fread(k,1,'char');

fseek(k,3102,-1);
calib=fread(k,1,'char');

fseek(k,3103,-1);
pxl=fread(k,calib,'double');


fseek(k,3183,-1);
wvd=fread(k,calib,'double');
disp(num2str([pxl wvd] ))

fseek(k,3263,-1);
polycoef=fread(k,polyorder+1,'double')
polycoef=polycoef(length(polycoef):-1:1);
wvn=polyval(polycoef,[1:xdim]);

fseek(k,3311,-1);
lsrline=fread(k,1,'double')
if lsrline==0
    % user forgot to enter laserline
    lsrline=785;
    disp('Laser Line Corrected to 785nm, hope its right!');
end
wvn=1/lsrline*1e7-(1./wvn*1e7);
fseek(k,4100,-1);
b=fread(k,xdim*ydim,datatype);
b=reshape(b,xdim,ydim)';
% jank data into a wavenumber plot
if ydim==1
    if smpts>0
        b=medianfilt(b,smpts);
    end
    if bcPts>0
        b=smooth(b,2*bcPts+1); % boxcar filter
    end
    wvx=round(min(wvn)):round(max(wvn));
    wvy=interp1(wvn,b,wvx,'pchip'); % use pchip method for interpolation
end

fclose(k);

function [b,wvx,wvy]=ascread(filename,smpts,bcPts)
data=dlmread(filename,'\t');
wvn=data(:,1);
b=data(:,2);
if smpts>0
    b=medianfilt(b,smpts);
end
if bcPts>0
    b=smooth(b,2*bcPts+1); % boxcar filter
end
wvx=round(min(wvn)):round(max(wvn));
wvy=interp1(wvn,b,wvx,'pchip');

function y=medianfilt(x,pts)
y=x;
for k=(pts+1):(length(x)-pts)
    y(k)=median(x(k-pts:k+pts));
end



function baseline=lieberfake(data,showf)
%LieberFake Function
% By Kevin Mader (2007)
% Email (kevinmader@gmail.com)
%About
% Designed to use the Lieber algorithm to subtract the fluorescence and
% other background from the Raman Data. The basic idea is that background
% will be very broad while the Raman peaks will be very sharp
%Operation
% [baseline]=lieberfake(data,ShowFigure)
%INPUT Arguments
% data- the spectra to be finding the baseline for
% ShowFigure- plots the function at every step (slow), default 0 (off)
%OUTPUT Arguments
% baselin- the polynomial baseline in the same dimension as the input data
if nargin<2
    showf=0;
end
fdata=data;
if showf
    close all
end
d=1;
tic;
while d==1
    % fiz=fit([1:length(fdata)]',smooth(fdata,15),'pz*x^4+p*x^3+a*x^2+b*x+c','Startpoint',[0 0 0 0 data(1)]);
    fiz=smooth(fdata,.99,'sgolay',3);
    spotCount=sum(((fdata'-fiz(1:length(data)))<0));
    fdata=((fdata-fiz(1:length(data))')<0).*fdata+((fdata-fiz(1:length(data))')>=0).*fiz(1:length(data))';
    if showf
        plot([1:length(fdata)]',data,'b-',[1:length(fdata)]',fdata,'r-')
        pause(.1);
        toc
        spotCount=spotCount
    end
    if spotCount<.08*length(fdata) | toc>4
        d=0;
    end
end
% ndata=cumsum([fdata(1);(smooth(diff(fdata),170))]);
baseline=fdata';
if showf
    plot([1:length(fdata)]',data,'b-',[1:length(fdata)]',baseline,'r-',[1:length(fdata)],data-baseline','m-')
end



function runz=loadRunz(handles)
if get(handles.useCollapsed,'Value')
    runz=getappdata(handles.figure1,'Crunz');
else
    runz=getappdata(handles.figure1,'runz');
end

function writeRunz(handles,runz)
if get(handles.useCollapsed,'Value')
    setappdata(handles.figure1,'Crunz',runz);
else
    setappdata(handles.figure1,'runz',runz);
end

function showPeaks(handles,peaks)
hold on;
for i=1:size(peaks,1)
    line([peaks(i,1),peaks(i,1)],get(handles.axes1,'YLim'),'LineWidth',2,'Color','g')
    line([peaks(i,2),peaks(i,2)],get(handles.axes1,'YLim'),'LineWidth',2,'Color','r')
end

function outvalue=kevinSelList(titleStr,outStr,indata);
opts={};
j=find(outStr=='$');
if j>1
    preStr=outStr(1:(j-1));
else
    preStr='';
end
if j<length(outStr)
    postStr=outStr((j+1):end);
else
    postStr=[];
end
for i=1:length(indata)
    if isnumeric(indata(i))
        opts{i}=[preStr num2str(indata(i)) postStr];
    else
        opts{i}=[preStr indata(i) postStr];
    end
end
[s,v] = listdlg('PromptString',titleStr,'ListString',opts);
if v==1
    outvalue=s;
else
    outvalue=[];
end

function collapseData(handles,peaks,isEdit)
% this function collapses a spectra full of data into a series of points
% representing important peaks or regions

set(handles.useCollapsed,'Value',0);
runz=loadRunz(handles);
if ~exist('isEdit','var')
    isEdit=0;
end
if ~exist('peaks','var');
    peaks=[];
end

PRGMODE=0;
if isempty(peaks)
    PRGMODE=1; % new peak mode
elseif isEdit==0
    PRGMODE=2; % blind reintegrate mode
else
    PRGMODE=3; % edit mode
end
if PRGMODE==1 | PRGMODE==3


    set(handles.plot_GrpAvg,'Value',1);

    updatePlots(handles);
    if PRGMODE==3
        showPeaks(handles,peaks);
        delPeaks=kevinSelList('Select Peaks (if any) you would like to delete...','Peak $.pks',mean(peaks'));
        peaks(delPeaks,:)=[];
        updatePlots(handles);
        showPeaks(handles,peaks);
    end


    set(handles.txt_Status,'String','Click the Left Side and then the Right Side of Each Peak/Region You wish to have included in the analysis. Green represents the left boundary and red represents the right boundary');

    stillPicking=1;
    waveMap=runz.cellz{1}.relwavenumber;

    while stillPicking==1
        axes(handles.axes1);
        [x,junk]=ginput(2); % select two points;
        x=[min(x);max(x)]; % make sure the points are in the right order
        peaks=[peaks' x]'; % add the two points to the peak array
        % this little snippet of code will plot the points you just clicked so you can see what you've already integrated
        cpeak=[];
        [junk,cpeak(1)]=min(abs(x(1)-waveMap));
        [junk,cpeak(2)]=min(abs(x(2)-waveMap));

        line(runz.cellz{1}.relwavenumber([cpeak(1) cpeak(1)]),get(handles.axes1,'YLim'),'LineWidth',2,'Color','g')
        line(runz.cellz{1}.relwavenumber([cpeak(2) cpeak(2)]),get(handles.axes1,'YLim'),'LineWidth',2,'Color','r')

        %plot(runz.cellz{1}.relwavenumber(cpeak),runz.cellz{1}.avgPlot(cpeak),'k+');

        stillPicking=menu('Add Another Peak?','No','Yes')-1;
    end
    [junk,goodOrder]=sort(mean(peaks'));
    peaks=peaks(goodOrder,:); % sort the peaks so the results make sense
else
    loadOld=1;
end
smallRunz=runz;

for cel=1:length(smallRunz.cellz)
    waveMap=runz.cellz{cel}.relwavenumber;
    smallRunz.cellz{cel}.relwavenumber=[];
    avgSpec=[]; % calculate the average spectra while the main loop is running
    for peak=1:size(peaks,1)
        smallRunz.cellz{cel}.relwavenumber(end+1)=mean(peaks(peak,:));
        cpeak=[];
        [junk,cpeak(1)]=min(abs(peaks(peak,1)-waveMap));
        [junk,cpeak(2)]=min(abs(peaks(peak,2)-waveMap));
        curArea=[]; % this vector holds each of the areas so that the average spectra can be calculated at the end
        for dat=1:length(smallRunz.cellz{cel}.data)
            if peak==1
                smallRunz.cellz{cel}.data{dat}.spectrum=[];
            end
            curArea(end+1)=trapz(runz.cellz{cel}.data{dat}.spectrum(cpeak(1):cpeak(2)))/(cpeak(2)-cpeak(1)); % take the intergral and then divide by the distance across so all the numbers are on the same scale
            smallRunz.cellz{cel}.data{dat}.spectrum(end+1)=curArea(end); % the current point is the last point in curArea
        end
        avgSpec(end+1)=mean(curArea);
    end
    smallRunz.cellz{cel}.avgPlot=avgSpec;
end
if isfield(smallRunz,'statz')
    smallRunz=rmfield(smallRunz,'statz'); % make sure there is no PCA
end
if isfield(smallRunz,'clusterz')
    smallRunz=rmfield(smallRunz,'clusterz') % make sure there is no cluster
end
smallRunz.peaks=peaks; % save the peaklist inside the file justin case
CollapsedDone(handles);
set(handles.useCollapsed,'Value',1);
set(handles.plot_GrpAvg,'Value',1);
writeRunz(handles,smallRunz);
NoPCADone(handles);
NoClusterDone(handles);
updatePlots(handles);
set(handles.txt_Status,'String','');
set(handles.txt_Status,'String','');



function deleteFileGroup(handles,j)
curVal=get(handles.useCollapsed,'Value');
set(handles.useCollapsed,'Value',0);
runz=loadRunz(handles);
sDex=rewind(runz.cellz,j,1);
fDex=sDex-1+length(runz.cellz{j}.data);
runz.cellz{j}=[];
runz=updatePCA(runz);

if isfield(runz,'clusterz') % clustermode on
    if isfield(runz.clusterz,'grpId')
        runz.clusterz.grpId(sDex:fDex)=[];
    end
    if isfield(runz.clusterz,'grpIdGuess')
        runz.clusterz.grpIdGuess(sDex:fDex)=[];
    end
end
writeRunz(handles,runz);
if strcmp(get(handles.useCollapsed,'Visible'),'on')
    set(handles.useCollapsed,'Value',1);
    runz=loadRunz(handles);
    runz.cellz{j}=[];
    runz=updatePCA(runz);
    if isfield(runz,'clusterz') % clustermode on
        if isfield(runz.clusterz,'grpId')
            runz.clusterz.grpId(sDex:fDex)=[];
        end
        if isfield(runz.clusterz,'grpIdGuess')
            runz.clusterz.grpIdGuess(sDex:fDex)=[];
        end
    end
    writeRunz(handles,runz);
end
set(handles.useCollapsed,'Value',curVal);
updatePlots(handles);

function deleteSpectrum(handles,j,k)

curVal=get(handles.useCollapsed,'Value');
set(handles.useCollapsed,'Value',0);
runz=loadRunz(handles);
sDex=rewind(runz.cellz,j,k);
runz.cellz{j}.data(k)=[];
avgPlot=0;

for jj=1:length(runz.cellz{j}.data)
    avgPlot=avgPlot+runz.cellz{j}.data{jj}.spectrum;
end
runz.cellz{j}.avgPlot=avgPlot/length(runz.cellz{j}.data);
runz=updatePCA(runz);

if isfield(runz,'clusterz') % clustermode on
    if isfield(runz.clusterz,'grpId')
        try
            runz.clusterz.grpId(sDex)=[];
        catch
            runz.clusterz=rmfield(runz.clusterz,'grpId');
        end
    end
    if isfield(runz.clusterz,'grpIdGuess')
        try
            runz.clusterz.grpIdGuess(sDex)=[];
        catch
            % index wasn't there
            runz.clusterz=rmfield(runz.clusterz,'grpIdGuess');
        end
    end
end

if isfield(runz,'ramanimage')
    % raman image mode
    % delete the mapping and the impos
    runz.ramanimage.mapping=[]; % force the program to remake the mapping
    runz.ramanimage=rmfield(runz.ramanimage,'mapping');
    runz=RImakemap(runz);
    
    
    %runz.ramanimage.impos(k,:)=[];
end

writeRunz(handles,runz);
if strcmp(get(handles.useCollapsed,'Visible'),'on')
    set(handles.useCollapsed,'Value',1);
    runz=loadRunz(handles);
    runz.cellz{j}.data(k)=[];
    avgPlot=0;
    for jj=1:length(runz.cellz{j}.data)
        avgPlot=avgPlot+runz.cellz{j}.data{jj}.spectrum;
    end
    runz.cellz{j}.avgPlot=avgPlot/length(runz.cellz{j}.data);
    runz=updatePCA(runz);
    if isfield(runz,'clusterz') % clustermode on
        if isfield(runz.clusterz,'grpId')
            runz.clusterz.grpId(sDex)=[];
        end
        if isfield(runz.clusterz,'grpIdGuess')
            runz.clusterz.grpIdGuess(sDex)=[];
        end
    end
    writeRunz(handles,runz);
end
set(handles.useCollapsed,'Value',curVal);
if isfield(runz,'ramanimage')
    set(handles.txt_Status,'String','Click On Raman Image Before Performing Any More Actions');
end

updatePlots(handles);


function createGroups(handles,isTraining)
%this function lets users classify the data for either the training set or
%for comparison with the results calculated from the training set
runz=loadRunz(handles);
[runz,pcs]=updatePCA(runz);
runz.clusterz.pcs=pcs;

if ~exist('isTraining','var')
    isTraining=0;
end
if ~isfield(runz.clusterz,'grpId')
    runz.clusterz.grpId=[];
end
if length(runz.clusterz.grpId)<size(pcs,1)
    runz.clusterz.grpId=ones(1,size(pcs,1));
end
grpId=runz.clusterz.grpId;

if ~isfield(runz.clusterz,'training')

    runz.clusterz.PCKeep=10;
else
    PCKeep=runz.clusterz.PCKeep;
    try
        runz.clusterz.grpIdGuess=classify(runz.clusterz.pcs(:,[1:PCKeep]),runz.clusterz.training.pcs(:,[1:PCKeep]),runz.clusterz.training.grpId,'mahalanobis');
    catch
        runz.clusterz.grpIdGuess=[];
    end
end
writeRunz(handles,runz);
ClusterDone(handles);
set(handles.plot_Cluster,'Value',1);
updatePlots(handles);
fileG=menu('Would You like to Identify Groups By File Name?','No','Yes')-1;
xx=[];
yy=[];
if fileG
    set(handles.txt_Status,'String','Select the file in a given group then choose what group to put them in');
end
while fileG
    newGroup=max(grpId)+1;
    mastaList=makeFileNameList(runz,grpId);
    [s,v] = listdlg('PromptString',['Select Files to Put into Group #' num2str(newGroup)],'ListString',mastaList,'ListSize',[600 500]);
    if v==1
        grpId=identGroup(grpId,s);
    end
    runz.clusterz.grpId=grpId;
    writeRunz(handles,runz);
    updatePlots(handles);
    fileG=menu('Add another filename group?','No','Yes')-1;
end


% identify groups by polygon
polyG=(menu('Add a polygon group?','No','Yes')-1);
if polyG
    set(handles.txt_Status,'String','Please click the boundary for the area you would like to average. Right click the last point');
end
while polyG

    d=1;
    xd=[];
    yd=[];
    hd=[];
    while d==1
        [x,y,d]=ginput(1);
        xd(end+1)=x;
        yd(end+1)=y;
        if length(xd)>1
            hd(end+1)=line([xd(end-1),xd(end)],[yd(end-1),yd(end)],'Color',[1,0,0]);
        end
    end
    hd(end+1)=line([xd(1) xd(end)],[yd(1) yd(end)],'Color',[1,0,0]);
    xx=pcs(:,1);
    yy=pcs(:,2);
    % place points into group
    loc=[];
    for i=1:length(xx)
        if npoly(xd,yd,xx(i),yy(i))
            loc(end+1)=i;
        end
    end
    grpId=identGroup(grpId,loc);
    polyG=(menu('Add another polygon group?','No','Yes')-1);
end

% if this is a raman image, let the user identify peaks by spatial location
if isfield(runz,'ramanimage')
    % identify groups by polygon
    set(handles.plot_RamanImage,'Value',1);
        updatePlots(handles);
    polyG=(menu('Add a Raman Image-based polygon group?','No','Yes')-1);
    if polyG
        set(handles.txt_Status,'String','Please click the boundary for the area you would like to average. Right click the last point');
        
    else
        set(handles.plot_Cluster,'Value',1);
        updatePlots(handles);
    end
    while polyG
        d=1;
        xd=[];
        yd=[];
        hd=[];
        while d==1
            [x,y,d]=ginput(1);
            xd(end+1)=x;
            yd(end+1)=y;
            if length(xd)>1
                hd(end+1)=line([xd(end-1),xd(end)],[yd(end-1),yd(end)],'Color',[1,0,0]);
            end
        end
        hd(end+1)=line([xd(1) xd(end)],[yd(1) yd(end)],'Color',[1,0,0]);
        xx=runz.ramanimage.impos(:,1);
        yy=runz.ramanimage.impos(:,2);
        % place points into group
        loc=[];
        for i=1:length(xx)
            if npoly(xd,yd,xx(i),yy(i))
                loc(end+1)=mapping(i);
            end
        end
        grpId=identGroup(grpId,loc);
        updatePlots(handles);
        polyG=(menu('Add another RI polygon group?','No','Yes')-1);
    end
end

% fix individual points
pointG=(menu('Correct an single point?','No','Yes')-1);
if polyG
    set(handles.txt_Status,'String','Please click point whose group you wish to change');
end

while pointG
    [x,y,d]=ginput(1);
    xx=pcs(:,fstPC);
    yy=pcs(:,sndPC);
    [junk,loc]=min( (xx-x).^2+(yy-y).^2 ); % find the closest point
    grpId=identGroup(grpId,loc);
    runz.clusterz.grpId=grpId;
    writeRunz(handles,runz);
    updatePlots(handles);
    pointG=(menu('Correct another single point?','No','Yes')-1);
end
runz.clusterz.grpId=grpId; % best guess is still calibration data




writeRunz(handles,runz);
set(handles.plot_Cluster,'Value',1);
updatePlots(handles);

function toKeep=keepOnly(grpId,pcnt)
toKeep=[];
for j=1:max(grpId)
    cGrp=find(grpId==j);
    if length(cGrp)>0
        cRnd=rand(length(cGrp),1);
        while ~length(find(cRnd<pcnt)) % make sure there is at least one element from the group
            cRnd=rand(length(cGrp),1);
        end
        cGrp=cGrp(:);
        toKeep=[toKeep; cGrp(find(cRnd<pcnt))];
    end
end
function mastaList=makeFileNameList(runz,grpId)
mastaList={};
for j=1:length(runz.cellz)
    for k=1:length(runz.cellz{j}.data)
        cGrp=grpId(length(mastaList)+1);
        if cGrp>0
            grpText=[' (Group #' num2str(cGrp) ')'];
        else
            grpText=[' (Unclassified)'];
        end
        mastaList{end+1}=[runz.cellz{j}.name{1} ':' runz.cellz{j}.data{k}.name grpText];
    end
end


function grpId=identGroup(grpId,loc)
grps={'Do not classify'};
for j=1:max(grpId)
    grps{j+1}=['Group #' num2str(j)];
    if j==grpId(loc)
        grps{j+1}=[grps{j+1} ' (Current Group)'];
    end

end
grps{max(grpId)+2}=['Group #' num2str(max(grpId)+1) ' (New Group)'];
cVal=menu('Which Group Should This Point Be In?',grps);
if cVal~=0
    if ~isempty(loc)
        grpId(loc)=cVal-1;
    end
end

function [sens,spec]=SensAndSpec(grpId,grpIdG)
badEle=find(grpId==0);
grpId(badEle)=[]; % items you dont classify dont count against the tool
grpIdG(badEle)=[];
for i=1:max(grpId)

    cGrp=(grpId(:)==i);
    gGrp=(grpIdG(:)==i);

    sens(i)=sum(cGrp & gGrp)/(sum(cGrp & gGrp)+sum((cGrp-gGrp)>0)); % in both guess and true data / (in both guess and true data +
    spec(i)=sum(~cGrp & ~gGrp)/(sum(~cGrp & ~gGrp)+sum((gGrp-cGrp)>0));
end


function liveImage(handles)
% this function is made to allow raman imaging to perform a neat live image
% feature
runz=loadRunz(handles);
%if ~isfield(runz.ramanimage,'data')
datamatrix=[];
tempMat=data2mat(runz.cellz{1}.data);
datamatrix=[datamatrix; tempMat];
runz.ramanimage.data=datamatrix-repmat(mean(datamatrix),size(datamatrix,1),1);
%end
set(handles.plot_GrpAvg,'Value',1)
updatePlots(handles);


    xd=sort(runz.ramanimage.impos(:,1));
    yd=sort(runz.ramanimage.impos(:,2));
    xMin=xd(1)
    xMax=xd(end);
    yMin=yd(1);
    yMax=yd(end);
    xSteps=min([60 length(find([1;diff(xd)]>0))]);
    ySteps=min([60 length(find([1;diff(yd)]>0))]);
    xSteps(2)=128;
    ySteps(2)=128;
    xSteps(3)=512;
    ySteps(3)=512;
    nSteps=menu('Select Resolution :',[num2str(xSteps(1)) ' x ' num2str(ySteps(1))],[num2str(xSteps(2)) ' x ' num2str(ySteps(2))],[num2str(xSteps(3)) ' x ' num2str(ySteps(3))]);
    if nSteps<1
        return
    else
        xSteps=xSteps(nSteps);
        ySteps=ySteps(nSteps);
    end
    
    runz.ramanimage.liveimage.xBnds=[xMin:(xMax-xMin)/(xSteps-1):xMax];
    runz.ramanimage.liveimage.yBnds=[yMin:(yMax-yMin)/(ySteps-1):yMax];
    runz.ramanimage.liveimage.img=zeros(length(runz.ramanimage.liveimage.xBnds),length(runz.ramanimage.liveimage.yBnds),1);

    
    
    runz.ramanimage.relwavenumber=runz.cellz{1}.relwavenumber;
    %[x,y]=meshgrid(runz.ramanimage.liveimage.yBnds,runz.ramanimage.liveimage.xBnds);
    %for curDat=1:size(liData.data,2)
    %    for i=1:length(liData.impos(:,1))
    %        if liData.mapping(i)<=size(liData.data,1) & liData.mapping(i)>0
    %            liData.liveimage.img(:,:,curDat)=liData.liveimage.img(:,:,curDat)+liData.data(liData.mapping(i),curDat)*exp(-((x-liData.impos(i,1)).^2+(y-liData.impos(i,2)).^2)/(5^2));
    %        end
    %    end
        % for i=1:length(liData.liveimage.xBnds)
        %     x=liData.liveimage.xBnds(i);
        %     for j=1:length(liData.liveimage.yBnds)
        %         y=liData.liveimage.yBnds(j);
        %         [junk curFile]=min((x-liData.impos(:,1)).^2+(y-liData.impos(:,2)).^2);
        %         liData.liveimage.img(i,j,curDat)=liData.data(liData.mapping(curFile),curDat);
        %     end
        %end
    %    disp([num2str(round(curDat/size(liData.data,2)*100)) '% Complete creating Image Matrix']);
    %    pause(.01);
    %end
    %runz.ramanimage=liData;
    %liData=runz.ramanimage;

%liData=runz.ramanimage;
writeRunz(handles,runz);

RIslides(runz,menu('Display Axes?','Principal Components','Raw Spectra')==1);
%g=datacursormode(handles.figure1);
%set(g,'Enable','on','DisplayStyle','datatip','SnapToData','off');
%set(handles.txt_Status,'String','Please Click the First Wavelength To make the Raman image at');
%disp('Please Click the First Wavelength To make the Raman image at');
%set(g,'UpdateFcn',@liUpdate);
%figure(2);
%global curImg
%curImg=imagesc(liData.liveimage.xBnds,liData.liveimage.yBnds,liData.liveimage.img(:,:,1));





function pointClick(handles,x,y,d)
runz=loadRunz(handles);


ptz=getappdata(handles.figure1,'ptz');
set(handles.plot_PCS,'Value',1);
set(handles.NumberPlot,'Value',0);
newRun=0;
if isempty(ptz)
    newRun=1;
else
    if ptz<2
        newRun=1;
    else
        diffSpecSub=getappdata(handles.figure1,'diffSpecSub');
    end
end
if newRun==1
    updatePlots(handles);
    [a,b]=size(runz.cellz{1}.relwavenumber);
    diffSpecSub=zeros(a,b);
end
if get(handles.plot_RamanImage,'Value')
    xx=runz.ramanimage.impos(:,1);
    yy=runz.ramanimage.impos(:,2);
    %elseif get(handles.plot_PCS,'Value')
    %    xx=runz.clusterz.pcs(:,1);
    %    yy=runz.clusterz.pcs(:,2);
else
    xx=getappdata(handles.figure1,'xx');
    yy=getappdata(handles.figure1,'yy');
end
ptz=getappdata(handles.figure1,'ptz');
axes(handles.axes1);
set(handles.txt_Status,'String','Please click the points you would like to view. Right Click to Delete.');

if nargin==1
    [x,y,d]=ginput(1);
end
if d==1 | d==3
    [junk,cpoint]=min((xx-x).^2+(yy-y).^2);
    cpoint=cpoint(1);
    if ~get(handles.plot_RamanImage,'Value')
        [j,k]=unwind(runz,cpoint);
    else
        j=1; % only one group allowed
        k=runz.ramanimage.mapping(cpoint);
    end
    if d==3

        hk1=plot(xx(cpoint),yy(cpoint),'k*','MarkerSize',12);
        sure=menu('Are you sure you want to delete this cell?','Yes','No');
        if sure==1
            deleteSpectrum(handles,j,k);

        else
            delete(hk1)
        end
    else
        if isfield(runz.cellz{j}.data{k},'name')
            gName=runz.cellz{j}.data{k}.name;
        else
            gName=num2str(k);
        end
        GraphName=['Group: ' runz.cellz{j}.name{1} ' Graph: ' gName];
        dplot(runz.cellz{j}.relwavenumber,runz.cellz{j}.data{k}.spectrum,GraphName,ptz);
    end

    ptz=ptz+1;

end
setappdata(handles.figure1,'ptz',ptz);
set(handles.txt_Status,'String','');





















