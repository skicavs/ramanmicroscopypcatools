function [ldComp,ldScores,grpIdG] = tylercluster(datatable,grpId)
%tylercluster (part of PCATools)
% By Kevin Mader
% Updated 08-15-07
%This function is made to cluster the data using PCA-LDA and using Tyler's
%Method for determining the number of PCs to use
%
%Input Variables
% datatable [files x numberOfWavelengs] this is the spectral data
% grpId [files x 1] this is the classification assigned in the program
%
%Output Variables
% ldComp [numberOfWavelengs x numOfPC] the components used to plot the data
% (eigenvectors)
% ldScores [numOfPC x files] the score table from the analysis
% (eigenvalues)
% grpIdG [files x 1] the classification determined by this algorithm

numObs=size(datatable,1);
V=1:numObs;
grpCnt=[];
for i=1:max(grpId)
    grpCnt(i)=length(find(grpId==i)); % makes a list of the number of elements in each group
end
maxComp=min([30 grpCnt-1]); % use up to 30 PCs if the groups are big enough

pCnt=[];
[coeff,score]=princomp(datatable(:,:),'econ');
for j=1:maxComp
    %grpIdG=zeros(length(V));
    pScore=zeros(length(V),j);
    
    coeffs=coeff(:,1:j); % delete the extra components
    scores=score(:,1:j); % delete the extra scores
    grpIdG=crossval('lda',scores,grpId,10);
    pCnt(j)=length(find(grpIdG==grpId'))/length(grpIdG);
end

for i = V
  [coeff,score]=princomp(datatable(V~=i,:),'econ'); % econ keeps the princomp from calculating null components
  coeff(:,(j+1):end)=[]; % delete the extra components
  score(:,(j+1):end)=[]; % delete the extra scores
  %cLDA=lda(score,grpId(V~=i));
  data.X=score';
  data.y=grpId(V~=i);
  cLDA=lda(data,1);
  
  %[d,p,stats]=manova1(score,grpId(V~=i));
  %cLDA=stats.eigenvec;
  cScore=datatable(V==i,:)*coeff;
  cLDS=cScore*cLDA.eigval;
  grpIdG(V==i)=classify(cScore,score,grpId(V~=i));
  pScore(V==i,:)=cLDS; %cScore*stats.eigenvec;
end
end